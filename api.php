
/*API*/


function CheckToken($param, $token='123'){
	if($param!=$token) exit("Poor token");
}

function redirect($url) {
		header('location: '.$url);
		exit;
}
/*
*GET
*/

if($module=='get') {



	
	CheckToken($Param['token'], $api_token);

	/*
	*GET ORDERS DATA
	*/
	if(!empty($Param['orders'])) {

		if($Param['orders']=='all') {

			if(!empty($Param['status'])) {

				if($Param['status']=='nonagree') {

					$result = mysqli_query($CONNECT, 'SELECT * FROM `orders` WHERE `agree`=0 AND `disagree`=0');
					if($result)
						while($row = mysqli_fetch_assoc($result))
				    		$data[] = $row; 

				} elseif($Param['status']=='agree') {

					$result = mysqli_query($CONNECT, 'SELECT * FROM `orders` WHERE `agree`=1');
					if($result)
						while($row = mysqli_fetch_assoc($result))
				    		$data[] = $row; 

				} elseif($Param['status']=='disagree') {

					$result = mysqli_query($CONNECT, 'SELECT * FROM `orders` WHERE `agree`=0 AND `disagree`=0');
					if($result)
						while($row = mysqli_fetch_assoc($result))
				    		$data[] = $row; 

				} 

			}



			if(!empty($data)) exit(json_encode($data));
			
		}

	}elseif(!empty($Param['order'])) {

		if(!empty($Param['count'])) {

			$instagram = new \InstagramScraper\Instagram();
			$account = $instagram->getAccount($Param['order']);
			if($Param['count'] <= $account->getMediaCount()) {

				$medias = $instagram->getMedias($Param['order'], $Param['count']);
				exit(json_encode($medias));

			}
			

		}
		

	}




} elseif($module=='update') {
/*
*UPDATE
*/

	CheckToken($Param['token'], $api_token);

	if (!empty($Param['order'])) {

		
		if(!empty($Param['agree'])) {
		/*
		*Agree
		*/
			mysqli_query($CONNECT, 'UPDATE `orders` SET `agree`="'.$Param["agree"].'" WHERE `username`="'.$Param["order"].'" ');
			header("Location: /panel");
			die();
		} elseif(!empty($Param['disagree'])){
		/*
		*Disagree
		*/
			mysqli_query($CONNECT, 'UPDATE `orders` SET `disagree`="'.$Param["disagree"].'" WHERE `username`="'.$Param["order"].'" ');
			redirect($_SERVER['HTTP_REFERER']);

		} elseif(!empty($Param['paid'])) {
		/*
		*Paid
		*/
			mysqli_query($CONNECT, 'UPDATE `orders` SET `paid`="'.$Param["paid"].'" WHERE `username`="'.$Param["order"].'" ');
			redirect($_SERVER['HTTP_REFERER']);
		}

	}


} elseif($module=='delete') {
/*
*delete
*/
	CheckToken($Param['token'], $api_token);
	if(!empty($Param['order'])) {
		mysqli_query($CONNECT, 'DELETE FROM `orders` WHERE `username`="'.$Param["order"].'" ');
		redirect($_SERVER['HTTP_REFERER']);
	}

}
